require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:product) { create(:product) }
    let(:product_property) { create(:product_property, product: product) }

    before do
      get :show, params: { id: product.id }
    end

    it "responds succsess" do
      expect(response).to be_successful
    end

    it "returns http 200" do
      expect(response).to have_http_status 200
    end

    it "returns show template" do
      expect(response).to render_template :show
    end

    it "assigns correct product" do
      expect(assigns(:product)).to eq product
    end

    it "assigns correct product_properties" do
      expect(assigns(:product_properties)).to match_array product_property
    end

    it "responds success with correct friendly_id" do
      get :show, params: { id: product.slug }
      expect(response).to be_successful
    end
  end
end
