module Potepan
  module ProductsHelper
    def options_text(product)
      variants = product.variants.includes(option_values: :option_type).with_prices(current_pricing_options).select do |variant|
        variant.option_values.any?
      end

      variants.map do |variant|
        values = variant.option_values.sort_by do |option_value|
          option_value.option_type.position
        end

        values.to_a.map! do |ov|
          "#{ov.option_type.presentation}: #{ov.presentation}"
        end

        values.to_sentence({ words_connector: ", ", two_words_connector: ", " })
      end
    end

    def quantities(product)
      product.stock_items.first.count_on_hand.times.map do |q|
        [q + 1, q + 1]
      end
    end
  end
end
