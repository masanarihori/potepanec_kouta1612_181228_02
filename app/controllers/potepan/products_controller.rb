class Potepan::ProductsController < ApplicationController
  include Spree::Core::ControllerHelpers::Pricing
  include Spree::Core::ControllerHelpers::Store

  def show
    @product = Spree::Product.friendly.find(params[:id])
    @images = @product.has_variants? ? @product.variant_images : @product.images
    @product_properties = @product.product_properties.includes(:property)
  end
end
